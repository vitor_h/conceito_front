module.exports = {
    projects: [
        {
            displayName: "Unit Tests",

            testMatch: ["<rootDir>/testes/**/*.test.js"],
        },
    ],
    reporters: ['default', "jest-junit"]
};